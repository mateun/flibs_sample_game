#version 430 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec4 color;
layout (location = 2) in vec2 uvs;

layout(location = 5) uniform mat4 mat_model;
layout(location = 6) uniform mat4 mat_proj;

out vec4 vcol;
out vec2 fs_uvs;

void main() {
  gl_Position = mat_proj * mat_model * (vec4(position, 1));
  vcol = color;
  fs_uvs = uvs;
  

}

