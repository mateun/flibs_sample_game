// f_libs.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "include/f_file.h"
#include "include/f_gl.h"
#include "include/f_math.h"
#include <SDL.h>
#include "games/koenig/mapping.h"
#include "games/koenig/koenig_gameplay.h"

#pragma comment(lib, "SDL2.lib")
#pragma comment(lib, "SDL2main.lib")

FVAO vaoRect;
FShader s;
FTexture texBlockGrass;
FTexture tHero;
FTexture texTerrainTiles;
KoenigMap koenigMap;

uint8_t blockMap[] = {
        1, 1, 0, 1,
        0, 1, 1,1,
        1, 0, 1,1,
        1, 1, 0, 1

};






void test_math() {
    V4 v = { 1, 2, 3, 4 };
    M4 m = {
        { 1, 0, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 1, 0},
        {0, 0, 0, 1}
    };

    V4 vr = m * v;

    M4 m2 = {
        { 98, 100, 100, 200},
        { 48, 600, 101, 300},
        { 38, 400, 102, 400},
        { 28, 500, 103, 500}
    };

    M4 mr = m2 * m;
    M4 mr2 = m * m2;

}

void test_f_file() {
    std::string s = f_read_file("E:/Projects/C++/f_libs/testfile.txt");
    printf("content: %s\n", s.c_str());
}



FVBuffer createVertexBufferTri() {
    std::vector<float> pos = {
        -0.5, -0.5, 1,
        0.5, -0.5, 1,
        0, 0.5, 1
    };
    FVBuffer vb;
    FGLResult result = f_createVertexBuffer(pos, &vb);
    if (!result.ok) {
        exit(5);
    }

    f_setVertexAttribute(0, GL_FLOAT, 3);

    std::vector<float> cols = {
        1, 0, 0, 1,
        0, 0, 1, 1,
        1, 1, 0, 1,
        0, 1, 0, 1
    };

    FVBuffer cb;
    f_createVertexBuffer(cols, &cb);
    f_setVertexAttribute(1, GL_FLOAT, 4);

    return vb;

}

FIBuffer createIndexBufferTri() {
    std::vector<uint32_t> is = {
        0, 1, 2
    };
    FIBuffer ib;
    f_createIndexBuffer(is, &ib);
    return ib;
}

void drawBuffer(FVBuffer vb, FShader shader) {
    glBindBuffer(GL_ARRAY_BUFFER, vb._id);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) nullptr);
    glEnableVertexAttribArray(0);

    glUseProgram(shader._shaderId);

    glDrawArrays(GL_TRIANGLES, 0, 3);

}

void drawRect(FVBuffer vb, FIBuffer ib, FShader shader) {
    glBindBuffer(GL_ARRAY_BUFFER, vb._id);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) nullptr);
    glEnableVertexAttribArray(0);

    glUseProgram(shader._shaderId);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib._id);


    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void*) nullptr);

}

void drawBufferIndexed(FVBuffer vb, FIBuffer ib, FShader shader) {
    glBindBuffer(GL_ARRAY_BUFFER, vb._id);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) nullptr);
    glEnableVertexAttribArray(0);

    glUseProgram(shader._shaderId);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib._id);

    glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, (void*) nullptr);

}


void drawVAO(FVAO vao, FShader shader) {
    glBindVertexArray(vao.vaoId);
    glUseProgram(shader._shaderId);
    glDrawElements(GL_TRIANGLES, vao.numberOfVertices, GL_UNSIGNED_INT, (void*) nullptr);
}




// This scales the unit size quad to 
// the actual pixel size, in this exemplatory 
// case this is 64x64.
// Furthermore it moves the pivot point to the center of 
// the sprite.
M4 getModelMatrix() {
    M4 m = {
        {64, 0, 0, 32},
        {0, 64, 0, 32},
        {0, 0, 1, 0},
        {0, 0, 0, 1}
    };

    return m;

}


void drawBlocks() {
    M4 pm = getPixelAdjustmentMatrix(64, 64, 32, 32);
    M4 ms = f_mscale(1, 1, 1);
    

    for (int r = 3 ; r > -1; r--) {
        for (int col = 0; col < 4; col++) {
            uint8_t value = blockMap[col + r * 4];
            if (value == 1) {

                M4 m = f_mtrans(col * 65, 500 - r * 128, -2) * ms * pm;
                f_setMatrixUniform(5, m);
                drawVAO(vaoRect, s, texBlockGrass);
            }
        }
    }
    

}







//void test_f_gl() {
//    
//    if (SDL_Init(SDL_INIT_VIDEO) == -1) {
//        SDL_Log("error");
//        exit(1);
//    }
//
//    SDL_Window* window = SDL_CreateWindow("f_gl_test", 700, 400, 800, 600, SDL_WINDOW_OPENGL);
//    SDL_GLContext ctx = initGL(window);
//
//    initShaders();
//    initTextures();
//    initRectangleVAO();
//    
//    bool run = true;
//    while (run) {
//        SDL_Event e;
//        while (SDL_PollEvent(&e)) {
//            if (e.type == SDL_QUIT) {
//                run = false; 
//                break;
//            }
//        }
//
//        glClearColor(0.1, 0.01, 0.01, 1);
//        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//        M4 projM = getOrthoProjectionMatrix(0, 800, 0, 600, 0.01, 100);
//        f_setMatrixUniform(6, projM);
//
//        drawBlocks();
//        
//        M4 pm = getPixelAdjustmentMatrix(64, 64, 32, 32);
//        M4 m = f_mtrans(80, 175, -1.9999) * f_mscale(0.5, 0.5, 1) * pm;
//        f_setMatrixUniform(5, m);
//        drawVAO(vaoRect, s, tHero);
//
//        SDL_GL_SwapWindow(window);
//    }
//
//    SDL_GL_DeleteContext(ctx);
//    SDL_DestroyWindow(window);
//    SDL_Quit();
//}

void koenig() {
    int w = 480;
    int h = 480;
    KGameState gameState = {};
    gameState.gold = 100;
    gameState.stone = 50;
    gameState.wood = 80;
    gameState.map = new KoenigMap();
    gameState.map->terrainW = w;
    gameState.map->terrainH = h;
    gameState.map->terrain = createRandomTerrain(w, h);
    koenigMain(&gameState);
}


// Just a small testdriver for the header libs.
int main(int argc, char** args)
{
    std::cout << "f_libs test:\n";

    test_f_file();
    test_math();
    //test_f_gl();

    koenig();

    return 0;
}
