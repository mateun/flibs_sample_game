#pragma once
#include "mapping.h"
#include <SDL.h>
#include "../../include/f_gl.h"
#include <string>
#include "../../include/f_file.h"
#include <map>

struct KGameState {
	int stone;
	int wood;
	int gold;
	KoenigMap* map;
};

struct TextureStore {
    FTexture get(const std::string& name) {
        return _textureStore[name];
    }
    void put(const std::string& name, FTexture t) {
        _textureStore[name] = t;
    }

    std::map<std::string, FTexture> _textureStore;
};


TextureStore* initTextures() {
    TextureStore* ts = new TextureStore();

    FTexture t;
    SDL_Surface* surface = SDL_LoadBMP("E:/Projects/C++/f_libs/block_grass.bmp");
    f_createTextureRGBA(64, 64, surface->pixels, &t);
    SDL_FreeSurface(surface);
    ts->put("block_grass", t);

    FTexture th;
    surface = SDL_LoadBMP("E:/Projects/C++/f_libs/hero.bmp");
    f_createTextureRGBA(64, 64, surface->pixels, &th);
    SDL_FreeSurface(surface);
    ts->put("hero", th);

    FTexture terrainTiles;
    surface = SDL_LoadBMP("E:/Projects/C++/f_libs/assets/terrain_sheet_128.bmp");
    f_createTextureRGBA(128, 128, surface->pixels, &terrainTiles);
    SDL_FreeSurface(surface);
    ts->put("terrainTiles", terrainTiles);

    return ts;
}

FShader initShader() {
    std::string vs = f_read_file("E:/Projects/C++/f_libs/vshader.glsl");
    std::string fs = f_read_file("E:/Projects/C++/f_libs/fshader.glsl");
    FShader s;
    FGLResult result = f_createShader(vs, fs, &s);
    return s;

}

FIBuffer createIBRect() {
    std::vector<uint32_t> is = {
        0, 1, 2,
        0, 2, 3
    };
    FIBuffer ib;
    f_createIndexBuffer(is, &ib);
    return ib;
}

FVBuffer createVBRect(std::vector<float>* customUVS) {
    std::vector<float> pos = {
        -0.5, 0.5, -0.1,
        -0.5, -0.5, -0.1,
        0.5, -0.5, -0.1,
        0.5, 0.5, -0.1
    };
    FVBuffer vb;
    FGLResult result = f_createVertexBuffer(pos, &vb);
    f_setVertexAttribute(0, GL_FLOAT, 3);

    std::vector<float> col = {
        1, 1, 0, 1,
        0.5, 0.7, 0.3, 1,
        0, 0, 0.7, 1,
        1, 1, 0, 1
    };
    FVBuffer cb;
    f_createVertexBuffer(col, &cb);
    f_setVertexAttribute(1, GL_FLOAT, 4);



    std::vector<float> uvs = {
        0, 1,
        0, 0,
        1, 0,
        1, 1,
    };


    FVBuffer cuv;
    if (customUVS) {
        f_createVertexBuffer(*customUVS, &cuv);
    }
    else {
        f_createVertexBuffer(uvs, &cuv);
    }
    
    f_setVertexAttribute(2, GL_FLOAT, 2);

    return vb;

}


FVAO createVAO() {
    FVAO vao;
    f_createVAO(&vao);
    return vao;
}

// This is the basis for all sprites. 
// A 1 unit square rect, made out of 2 triangles.
FVAO initRectangleVAO() {
    FVAO vaoRect = createVAO();
    FVBuffer vbRect = createVBRect(nullptr);
    FIBuffer ibRect = createIBRect();
    vaoRect.numberOfVertices = 6;
    f_unbindVAO();
    return vaoRect;
}

FVAO initTerrainVAO(TerrainType type) {

    std::vector<float>* uvs = nullptr;
    switch (type) {
    case tt_grass: uvs = new std::vector<float>({ 0, 0, 0, 0.25, 0.25, 0.25, 0.25, 0 }); break;
    case tt_sand: uvs = new std::vector<float>({ 0.25, 0, 0.25, 0.25, 0.5, 0.25, 0.5, 0 }); break;
    case tt_water: uvs = new std::vector<float>({ 0.5, 0, 0.5, 0.25, 0.75, 0.25, 0.75, 0 }); break;
    default: uvs = new std::vector<float>({ 0, 0, 0, 1, 1, 1, 1, 0, }); break;
    };

   /* uvs = new std::vector<float>({ 
        0, 0,
        0, 0.25,
        0.25, 0.25,
        0.25, 0 });*/

    FVAO vaoRect = createVAO();
    FVBuffer vbRect = createVBRect(uvs);
    FIBuffer ibRect = createIBRect();
    vaoRect.numberOfVertices = 6;
    f_unbindVAO();
    return vaoRect;
}

SDL_GLContext initGL(SDL_Window* window) {

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GLContext ctx = SDL_GL_CreateContext(window);
    SDL_GL_SetSwapInterval(1);

    GLenum r = glewInit();
    if (r != GLEW_OK) {
        exit(2);
    }

    glEnable(GL_DEPTH_TEST);
    //glEnable(GL_CULL_FACE);
    //glCullFace(GL_BACK);
    //glFrontFace(GL_CCW);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
}

M4 getOrthoProjectionMatrix(float left, float right, float bottom, float top, float nearc, float farc) {
    float w = right - left;
    float h = top-bottom;
    float d = farc - nearc;
    M4 m = {
        {2/w,  0,   0,  -((right + left) / (right - left))},
      //  {0,   2/h,  0,  -((top + bottom) / (top-bottom))},
        {0,   1 /(0.5f* h),  0,  -((top + bottom) / (top - bottom))},
        {0,    0, -2/d, -((farc + nearc) / (farc - nearc))},
        {0,    0,   0,   1}
    };

    return m;

}

M4 getPixelAdjustmentMatrix(int w, int h, int pivotX, int pivotY) {
    return {
        {(float)w, 0, 0, (float)pivotX},
        {0, (float)h, 0, (float)pivotY},
        {0, 0, 1, 0},
        {0, 0, 0, 1}
    };


}


void drawVAO(FVAO vao, FShader shader, FTexture tex) {
    glBindVertexArray(vao.vaoId);

    glUseProgram(shader._shaderId);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex.texId);
    glDrawElements(GL_TRIANGLES, vao.numberOfVertices, GL_UNSIGNED_INT, (void*) nullptr);
}

void koenigMain(KGameState* gameState) {


    if (SDL_Init(SDL_INIT_VIDEO) == -1) {
        SDL_Log("error");
        exit(1);
    }

    SDL_Window* window = SDL_CreateWindow("f_gl_test", 300, 200, 1920, 1080, SDL_WINDOW_OPENGL);
    SDL_GLContext ctx = initGL(window);

    FShader s = initShader();
    TextureStore* textureStore = initTextures();
    FVAO vaoRect = initRectangleVAO();
    FVAO vaoGrass = initTerrainVAO(tt_grass);
    FVAO vaoSand = initTerrainVAO(tt_sand);
    FVAO vaoWater = initTerrainVAO(tt_water);

    bool run = true;

    int scrollOffsetH = 0;
    int scrollOffsetV = 0;

    TerrainChunk* chunks = nullptr;
    int chunkSize = 80;
    chunks = createChunksForTerrain(gameState->map->terrainW, gameState->map->terrainH, gameState->map->terrain, chunkSize, chunkSize);

    while (run) {
        SDL_Event e;
        while (SDL_PollEvent(&e)) {
            if (e.type == SDL_QUIT) {
                run = false;
                break;
            }

            if (e.type == SDL_KEYDOWN) {
                if (e.key.keysym.sym == SDLK_r) {
                    delete[](gameState->map->terrain);
                    gameState->map->terrain = createRandomTerrain(gameState->map->terrainW, gameState->map->terrainH);
                    chunks = createChunksForTerrain(gameState->map->terrainW, gameState->map->terrainH, gameState->map->terrain, chunkSize, chunkSize);
                }

                if (e.key.keysym.sym == SDLK_LEFT) {
                    scrollOffsetH++;
                }
                if (e.key.keysym.sym == SDLK_RIGHT) {
                    scrollOffsetH--;
                }

                if (e.key.keysym.sym == SDLK_UP) {
                    scrollOffsetV--;
                }
                if (e.key.keysym.sym == SDLK_DOWN) {
                    scrollOffsetV++;
                }
            }

            
        }

        glClearColor(0.1, 0.01, 0.01, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        M4 projM = getOrthoProjectionMatrix(0, 1920, 1080 , 0, 0.01, 100);
        f_setMatrixUniform(6, projM);

        V4 tv = { 80, 40, -1, 1 };
        V4 rv = projM * tv;

        // Terrain
        M4 pm = getPixelAdjustmentMatrix(32 * chunkSize, 32 * chunkSize, 0, 0);
        M4 m = f_mtrans(380, 275, -1.2)  * pm;
        f_setMatrixUniform(5, m);
        //drawVAO(vaoWater, s, textureStore->get("terrainTiles"));   

        // Test render the first chunk
        int spacer = chunkSize * 32 * 0.5;
        int chunkCount = gameState->map->terrainH / chunkSize;
        for (int chr = 0; chr < chunkCount; chr++) {
            for (int chc = 0; chc < chunkCount; chc++) {
                M4 m = f_mtrans(100 + (scrollOffsetH * 33) + (spacer * chc), 300 + (-scrollOffsetV * 33) + (spacer * chr), -1.2) * f_mscale(0.5, 0.5, 1) * pm;
                f_setMatrixUniform(5, m);
                drawVAO(vaoRect, s, chunks[chc + chr * chunkCount].texture);
            }
        }
        

        /*for (int r = 0; r < gameState->map->terrainH; r++) {
            for (int c = 0; c < gameState->map->terrainW; c++) {
                m = f_mtrans(450 + (33*scrollOffsetH) + c * 30, 50 + (scrollOffsetV * 30) + r * 30, -5) * f_mscale(0.15, 0.15, 1) * pm;
                f_setMatrixUniform(5, m);

                TerrainType type = gameState->map->terrain[c + r * gameState->map->terrainW];
                switch (type) {
                case tt_grass: drawVAO(vaoGrass, s, textureStore->get("terrainTiles")); break;
                case tt_sand: drawVAO(vaoSand, s, textureStore->get("terrainTiles")); break;
                case tt_water: drawVAO(vaoWater, s, textureStore->get("terrainTiles")); break;
                    
                }
            }
        }*/
        
        // Hero
        pm = getPixelAdjustmentMatrix(64, 64, 32, 32);
        m = f_mtrans(80, 40, -1.9999) * f_mscale(0.5, 0.5, 1) * pm;
        f_setMatrixUniform(5, m);
        drawVAO(vaoRect, s, textureStore->get("hero"));

        SDL_GL_SwapWindow(window);
    }

    SDL_GL_DeleteContext(ctx);
    SDL_DestroyWindow(window);
    SDL_Quit();
}
