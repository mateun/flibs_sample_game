#pragma once

#include <inttypes.h>
#include <cstdlib>
#include "../../include/f_gl.h"


enum TerrainType {
	tt_grass,
	tt_sand, 
	tt_water
};

struct TerrainChunk {
	FTexture texture;
};

struct KoenigMap {
	TerrainType* terrain;
	uint32_t terrainW;
	uint32_t terrainH;
	TerrainChunk* chunks;

};




TerrainChunk* createChunksForTerrain(int w, int h, TerrainType* terrainTiles, int chunkSizeH, int chunkSizeV) {

	
	int horizontalChunks = w / chunkSizeH;
	int verticalChunks = h / chunkSizeV;

	TerrainChunk* chunks = new TerrainChunk[horizontalChunks * verticalChunks];


	SDL_Surface *surf = SDL_LoadBMP("E:/Projects/C++/f_libs/assets/terrain_sheet_128.bmp");
	SDL_Rect r_grass;
	r_grass.x = 0;
	r_grass.w = 32;
	r_grass.y = 0;
	r_grass.h = 32;

	SDL_Rect r_sand;
	r_sand.x = 32;
	r_sand.w = 32;
	r_sand.y = 0;
	r_sand.h = 32;


	SDL_Rect r_water;
	r_water.x = 64;
	r_water.w = 32;
	r_water.y = 0;
	r_water.h = 32;

	for (int v = 0; v < verticalChunks; v++) {
		for (int ch = 0; ch < horizontalChunks; ch++) {
			SDL_Surface* chunkSurf = SDL_CreateRGBSurface(0, chunkSizeH*32, chunkSizeV*32, 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);
			for (int r = 0; r < chunkSizeV; r++) {
				for (int c = 0; c < chunkSizeH; c++) {
					int pos = c + (ch * chunkSizeH) + (r+ (v * chunkSizeV)) * w;
					SDL_Rect dr;
					dr.x = c * 32;
					dr.w = 32;
					dr.h = 32;
					dr.y = r * 32;

					TerrainType type = terrainTiles[pos];
					if (type == tt_grass) {
				//		SDL_Log("row: %d\tcol: %d pos: %d \ttype: %d\n", r, c, pos, type);
						SDL_BlitSurface(surf, &r_grass, chunkSurf, &dr);
					}

					if (type == tt_sand) {
				//		SDL_Log("row: %d\tcol: %d pos: %d \ttype: %d\n", r, c, pos, type);
						SDL_BlitSurface(surf, &r_sand, chunkSurf, &dr);
					}

					if (type == tt_water) {
			//			SDL_Log("row: %d\tcol: %d pos: %d \ttype: %d\n", r, c, pos, type);
						SDL_BlitSurface(surf, &r_water, chunkSurf, &dr);
					}
				}
			}
			FTexture chunkTex;
			f_createTextureRGBA(chunkSizeH*32, chunkSizeV*32, chunkSurf->pixels, &chunkTex);
			chunks[ch + v * horizontalChunks] = {chunkTex};
		}
	}

	return chunks;
}


TerrainType* createRandomTerrain(int w, int h) {
	int size = w * h;
	TerrainType* terrainTiles = new TerrainType[size];
	for (int i = 0; i < size; i++) {
		terrainTiles[i] = tt_water;
	}

	srand(time(NULL));

	// Some splats of land, one island for now
	int landCol = rand() % w/2;
	int landRow = rand() % h/2  ;


	for (int r = 0; r < h/4; r++) {
		for (int c = 0; c < w/4; c++) {
			int pos = (landCol + c) + (landRow + r) * w;
			if (pos < size) {
				terrainTiles[pos] = tt_grass;
			}
		}
	}

	// Draw additions splats, randomly offset
	for (int s = 0; s < w/4; s++) {
		int offsetH = rand() % w/4 + s;
		int offsetV = rand() % w/4 + s;
		for (int r = 0; r < h/4; r++) {
			for (int c = 0; c < w/4; c++) {
				int pos = (offsetH + landCol + c) + (offsetV + landRow + r) * w;
				if (pos < size) {
					terrainTiles[pos] = tt_grass;
				}
			}
		}

	}
	
	return terrainTiles;
}
