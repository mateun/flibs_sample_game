#version 430 core

layout(binding = 0) uniform sampler2D sampler;

out vec4 color;

in vec4 vcol;
in vec2 fs_uvs;

void main() {

	color = texture(sampler, fs_uvs);

	if (color.r == 1 && color.g == 1 && color.b == 1 || 
		(color.r == 0 && color.g == 0 && color.b == 0)) {
		color.a == 0;
	}
	else {
		color.a = 1;
	}

	//color =  vec4(1, 1, 0, 1);
	//color *= vcol;
	
}
